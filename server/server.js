const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const cors = require('cors')

const sequelize = new Sequelize('music_management', 'root', 'info20schema14', {
    dialect : 'mysql',
    define : {
        timestamps : false
    }
})

const Melody = sequelize.define('melody', {
    singer : Sequelize.STRING,
    title : Sequelize.TEXT
}, {
    undescored : true
})

const Library = sequelize.define('library', {
    name : Sequelize.STRING,
    description : Sequelize.TEXT
})

Library.hasMany(Melody)

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(express.static('../music_management/build'))

app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'created'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/library', async (req, res) => {
	try{
		let libraries = await Library.findAll()
		res.status(200).json(libraries)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})


app.post('/libraries', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await Library.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await Library.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/libraries/:id', async (req, res) => {
	try{
		let library = await Library.findById(req.params.id)
		if (library){
			res.status(200).json(library)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/libraries/:id', async (req, res) => {
	try{
		let library = await Library.findById(req.params.id)
		if (library){
			await library.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/libraries/:id', async (req, res) => {
	try{
		let library = await Library.findById(req.params.id)
		if (library){
			await library.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/libraries/:lid/melodies', async (req, res) => {
	try{
		let library = await Library.findById(req.params.lid)
		if (library){
			let melodies = await library.getMelodies()
			res.status(200).json(melodies)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/libraries/:lid/melodies/:mid', async (req, res) => {
	try{
		let library = await Library.findById(req.params.lid)
		if (library){
			let melodies = await library.getMelodies({where : {id : req.params.mid}})
			res.status(200).json(melodies.shift())
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/libraries/:lid/melodies', async (req, res) => {
	try{
		let library = await Library.findById(req.params.lid)
		if (library){
			let melody = req.body
			melody.library_id = library.id
			await Chapter.create(melody)
			res.status(201).json({message : 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/libraries/:lid/melodies/:mid', async (req, res) => {
	try{
		let library = await Library.findById(req.params.lid)
		if (library){
			let melodies = await library.getMelodies({where : {id : req.params.mid}})
			let melody = melodies.shift()
			if (melody){
				await melody.update(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/libraries/:lid/melodies/:mid', async (req, res) => {
	try{
		let library = await Library.findById(req.params.lid)
		if (library){
			let melodies = await library.getMelodies({where : {id : req.params.mid}})
			let melody = melodies.shift()
			if (melody){
				await melody.destroy(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.listen(8080)