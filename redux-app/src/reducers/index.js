import { combineReducers } from 'redux'
import library from './library-reducer'

export default combineReducers({
    library
})
