const INITIAL_STATE = {
    libraryList : [],
    error : null,
    fetching : false,
    fetched : false
}
  
export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type){
        case 'GET_LIBRARIES_PENDING':
        case 'ADD_LIBRARY_PENDING':
        case 'UPDATE_LIBRARY_PENDING':
        case 'DELETE_LIBRARY_PENDING':
            return  {...state, error : null, fetching : true, fetched : false}           
        case 'GET_LIBRARIES_FULFILLED':
        case 'ADD_LIBRARY_FULFILLED':
        case 'UPDATE_LIBRARY_FULFILLED':
        case 'DELETE_LIBRARY_FULFILLED':                
            return {...state, libraryList : action.payload, error: null, fetched : true, fetching : false}
        case 'GET_LIBRARIES_REJECTED':
        case 'ADD_LIBRARY_REJECTED':
        case 'UPDATE_LIBRARY_REJECTED':
        case 'DELETE_LIBRARY_REJECTED':                                  
            return {...state, error : action.payload, fetching: false, fetched : false}
        default:
            break
    }
    return state
}