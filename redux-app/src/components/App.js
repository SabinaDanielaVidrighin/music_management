import React, {Component} from 'react'
import LibraryEditor from './LibraryEditor'

class App extends Component{
  render(){
    return <LibraryEditor />
  }
}

export default App
