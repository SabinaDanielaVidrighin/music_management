import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {DataTable} from 'primereact/datatable'
import {Column} from 'primereact/column'
import {Button} from 'primereact/button'
import {InputText} from 'primereact/inputtext'
import {Dialog} from 'primereact/dialog'

import {libraryActions} from '../actions'

const mapStateToProps = function(state) {
    return {
        libraryList : state.library.libraryList,
        loading : state.library.fetching
    }
}

const mapDispatchToProps = function(dispatch) {
    return {
      actions: bindActionCreators({
          getLibraries : libraryActions.getLibraries,
          addLibrary : libraryActions.addLibrary,
          updateLibrary : libraryActions.updateLibrary,
          deleteLibrary : libraryActions.deleteLibrary
      }, dispatch)
    }
}


class LibraryEditor extends Component{
    constructor(props){
        super(props)

        this.state = {
            isAddDialogShown : false,
            isNewLibrary : true,
            library : {
                title : '',
                content : ''
            }
        }

        this.deleteLibrary = (rowData) => {
            this.props.actions.deleteLibrary(rowData.id)
        }

        this.addNew = () => {
            let emptyLibrary = {
                title : '',
                content : ''
            }
            this.setState({
                isAddDialogShown : true,
                isNewLibrary : true,
                library : emptyLibrary
            })
        }

        this.hideAddDialog = () => {
            this.setState({
                isAddDialogShown : false
            })
        }

        this.editLibrary = (rowData) => {
            let libraryCopy = Object.assign({}, rowData)
            this.setState({
                library: libraryCopy,
                isNewLibrary : false,
                isAddDialogShown : true
            })
        }

        this.updateProperty = (property, value) => {
            let library = this.state.library
            library[property] = value
            this.setState({
                library : library
            })
        }

        this.opsTemplate = (rowData) => {
            return <>
                <Button icon="pi pi-times" className="p-button-danger" onClick={() => this.deleteLibrary(rowData)}  />
                <Button icon="pi pi-pencil" className="p-button-warning" onClick={() => this.editLibrary(rowData)} />
            </>
        }

        this.saveLibrary = () => {
            if (this.state.isNewLibrary){
                this.props.actions.addLibrary(this.state.library)
            }
            else{
                this.props.actions.updateLibrary(this.state.library.id, this.state.library)
            }
            this.setState({
                isAddDialogShown : false,
                library: {
                    title : '',
                    content : '',
                }
            })
        }

        this.tableFooter = <div>
            <span>
                <Button label="Add" onClick={this.addNew} icon="pi pi-plus" />
            </span>
        </div>

        this.addDialogFooter = <div>
            <Button   label="Save" icon="pi pi-save" onClick={() => this.saveLibrary()} />
        </div>
    }
    componentDidMount(){
        this.props.actions.getLibraries()
    }
    render(){
        let {libraryList} = this.props
        return <>
            <DataTable value={libraryList} footer={this.tableFooter} >
                <Column header="Title" field="title" />
                <Column header="Content" field="content" />
                <Column body={this.opsTemplate} />
            </DataTable>
            {
                this.state.isAddDialogShown ?
                <Dialog visible={this.state.isAddDialogShown} header="Add a library" onHide={this.hideAddDialog} footer={this.addDialogFooter} >
                     <InputText onChange={(e) => this.updateProperty('title', e.target.value)} value={this.state.library.title} name="title" placeholder="title" />
                    <InputText onChange={(e) => this.updateProperty('content', e.target.value)} value={this.state.library.content} name="content" placeholder="content" />
                </Dialog> 
                :
                null
            }
        </>
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LibraryEditor)