import {SERVER} from '../config/global'

export const GET_LIBRARIES = 'GET_LIBRARIES'
export const ADD_LIBRARY = 'ADD_LIBRARY'
export const UPDATE_LIBRARY = 'UPDATE_LIBRARY'
export const DELETE_LIBRARY = 'DELETE_LIBRARY'

export function getLibraries() {
    return {
      type: GET_LIBRARIES,
      payload: async () => {
          let response  = await fetch(`${SERVER}/libraries`)
          let json = await response.json()
          return json
      }
    }
}

export function addLibrary(library){
    return {
      type : ADD_LIBRARY,
      payload : async () => {
          await fetch(`${SERVER}/libraries`, {
              method : 'post',
              headers : {
                  'Content-Type' : 'application/json'
              },
              body : JSON.stringify(library)
          })
          let response  = await fetch(`${SERVER}/libraries`)
          let json = await response.json()
          return json
      }
    }
  }

  export function updateLibrary(libraryId, library){
    return {
        type : UPDATE_LIBRARY,
        payload : async () => {
            await fetch(`${SERVER}/libraries/${libraryId}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(library)
            })
            let response  = await fetch(`${SERVER}/libraries`)
            let json = await response.json()
            return json
        }
    }
}

export function deleteBook(libraryId){
    return {
        type : DELETE_LIBRARY,
        payload : async () => {
            await fetch(`${SERVER}/libraries/${libraryId}`, {
                method : 'delete'
            })
            let response  = await fetch(`${SERVER}/libraries`)
            let json = await response.json()
            return json
        }
    }
}


  